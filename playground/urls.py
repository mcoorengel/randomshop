from django.urls import path 
from . import views 

#URLConf
urlpatterns = [
    path('home/', views.home),
    path('home/index.html', views.home),
    path('home/products.html', views.products),
    path('home/contact.html', views.contact),
    path('home/shoppingcard.html', views.shoppingcard),
    path('home/thankyou.html', views.thankyou),
]