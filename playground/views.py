from django.shortcuts import render
from django.http import HttpResponse
# Create your views here.

def home(request):
    return render(request, 'index.html')

def products(request):
    return render(request, 'products.html')

def contact(request):
    return render(request, 'contact.html')

def shoppingcard(request):
    return render(request, 'shoppingcard.html')

def thankyou(request):
    return render(request, 'thankyou.html')